package cpsc2150.MyDeque;

/**
 * A generic Deque containing items of type T.
 * A deque is a data structure a double-ended queue that allows you
 * to insert and remove from both ends.
 * This deque is bounded by MAX_LENGTH
 *
 * Initialization ensures: the deque is empty
 *
 * Defines: length: Z
 *
 * Constraints: 0 <= length <= MAX_LENGTH
 *
 * @author Jacob Collins
 * @author Nicholas Childers
 */
public interface IDeque<T> {
    int MAX_LENGTH = 100;

    /**
     * Adds an item to the back of the deque
     * @param x the item to add to the back queue;
     * @pre length() < MAX_LENGTH;
     * @post removeLast() = x and length() = #length() + 1;
     */
    void enqueue(T x);

    /**
     * Removes the item from the front of the deque
     * @return the item removed from the front of the deque;
     * @pre length() > 0;
     * @post length() = #length() - 1 and the second item in the deque is now the first item;
     */
    T dequeue();

    /**
     * Adds an item to the front of the deque
     * @param x the item to add to the front of the deque;
     * @pre length() < MAX_LENGTH;
     * @post dequeue() = x and length() = #length() + 1;
     */
    void inject(T x);

    /**
     * Removes the item from the back of the deque
     * @return the item removed from the back of the deque;
     * @pre length() > 0;
     * @post length() == #length() - 1 and the second-to-last item in the deque is now the last item
     */
    T removeLast();

    /**
     * Gets the length of the deque
     * @return the number of items in the deque
     * @post self = #self
     */
    int length();

    /**
     * Removes all items from the deque
     * @post length() == 0;
     */
    void clear();

    /**
     * Returns the item at the front of the deque
     * @return the T at the front of the deque
     * @pre length() > 0
     * @post The Deque contains the same elements before and after the operation
     */
    default T peek() {
        T tmp = this.dequeue();
        this.inject(tmp);
        return tmp;
    }

    /**
     * Returns the item at the end of the deque
     * @return the T at the end of the deque
     * @pre length > 0
     * @post The Deque contains the same elements before and after the operation
     */
    default T endOfDeque() {
        T tmp = this.removeLast();
        this.enqueue(tmp);
        return tmp;
    }

    /**
     * Inserts c at position pos in the deque. 1 indexed
     * @param c the T to insert
     * @param pos the position to insert
     * @pre 0 < pos <= length() + 1 and length() < MAX_LENGTH
     * @post get(pos) = c and length() = #length() + 1
     */
    default void insert(T c, int pos)
    {
        if (pos == 1) {
            inject(c);
            return;
        }
        T tmp = this.dequeue();
        this.insert(c, pos - 1);
        this.inject(tmp);
    }

    /**
     * Removes whatever item was in position pos in the deque and returns it. 1 indexed.
     * @param pos the position to remove the item from.
     * @return the T that was removed
     * @pre 0 < pos <= length() and length > 0
     * @post length() = #length - 1 and any items behind pos are moved forward one position
     */
    default T remove(int pos) {
        if (pos == 1) {
          return this.dequeue();
        }
        T tmp = this.dequeue();
        T tmp2 = this.remove(pos - 1);
        this.inject(tmp);
        return tmp2;
    }

    /**
     * Returns whatever item is in position  pos in the deque. 1 indexed
     * @param pos the position to get the item from.
     * @return the T at position pos
     * @pre 0 < pos <= length()
     * @post The Deque contains the same elements before and after the operation
     */
    default T get(int pos) {
        if (pos == 1) {
            return this.peek();
        }
        T tmp = this.dequeue();
        T tmp2 = this.get(pos - 1);
        this.inject(tmp);
        return tmp2;
    }

}
