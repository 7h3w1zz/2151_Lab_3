package cpsc2150.MyDeque;

/**
 * Correspondence length = myLength
 * Correspondence self = myQ[0...myLength - 1]
 *
 * @invariant 0 <= myLength <= MAX_LENGTH
 *
 * @author Jacob Collins
 * @author Nicholas Childers
 */
public class ArrayDeque<T> extends AbsDeque<T> {
    // where the data is stored. myQ[0] is the front of the deque
    private final T[] myQ;

    // tracks how many items in the deque
    // also used to find the end of the deque
    private int myLength;

    /**
     * @post myQ is an empty T array
     */
    public ArrayDeque() {
        myQ = (T[]) new Object[IDeque.MAX_LENGTH];
        myLength = 0;
    }

    public void enqueue(T x) {
        myQ[myLength] = x;
        myLength++;
    }

    public T dequeue() {
        T tmp = myQ[0];
        for (int i = 0; i < myLength - 1; i++)
        {
            myQ[i] = myQ[i + 1];
        }
        myLength--;
        return tmp;
    }

    public void inject(T x) {
        for (int i = myLength; i > 0; i--)
        {
            myQ[i] = myQ[i - 1];
        }
        myLength++;
        myQ[0] = x;
    }

    public T removeLast() {
        myLength--;
        return myQ[myLength];
    }

    public int length() {
        return myLength;
    }

    public void clear() {
        myLength = 0;
    }
}
