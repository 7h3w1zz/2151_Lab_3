package cpsc2150.MyDeque;

import java.util.Scanner;

/**
 * Main class to test implementations of IDeque
 * @author Jacob Collins
 * @author Nicholas Childers
 * @invariant myScanner is an open Scanner
 */
public class IntegerDequeApp {

    private static IDeque<Integer> q;

    // Standard input
    private static final Scanner myScanner = new Scanner(System.in);

    // Error messages
    private static final String dequeFullErrorString = "Deque is full!";
    private static final String dequeEmptyErrorString = "Deque is empty!";
    private static final String dequePositionErrorString = "Not a valid position in the Deque!";

    /**
     * Main function to test implementations of IDeque
     * @param args array of string arguments passed to the application
     */
    public static void main (String[] args) {

        // Ask the user what class they want to use
        int input;
        while (true) {
            input = getUserInt("Enter 1 for array implementation or 2 for List implementation");

            // Instantiate the class that the user chose
            if (input == 1) {
                q = new ArrayDeque<>();
                break;
            } else if (input == 2) {
                q = new ListDeque<>();
                break;
            } else {
                System.out.println("Invalid class entered");
            }
        }

        // Main loop: ask user which action to perform, perform the action
        while (true) {

            // Ask the user what action they want to perform
            int menuSelection;
            while (true) {
                menuSelection = getUserInt("Select an option:\n" +
                        "1. Add to the end of the Deque\n" +
                        "2. Add to the front of the Deque\n" +
                        "3. Remove from the front of the Deque\n" +
                        "4. Remove from the end of the Deque\n" +
                        "5. Peek from the front of the Deque\n" +
                        "6. Peek from the end of the Deque\n" +
                        "7. Insert to a position in the Deque\n" +
                        "8. Remove from a position in the Deque\n" +
                        "9. Get a position in the Deque\n" +
                        "10. Get the length of the Deque\n" +
                        "11. Clear the Deque\n" +
                        "12. Quit");
                // If a valid menu item was selected, success!
                if (menuSelection > 0 && menuSelection <= 12) break;
                // Otherwise, error
                System.out.println("Not a valid option!");
            }

            // Figure out which option the user picked and perform
            //   appropriate action
            if      (menuSelection == 1)  addToEnd();
            else if (menuSelection == 2)  addToFront();
            else if (menuSelection == 3)  removeFromFront();
            else if (menuSelection == 4)  removeFromEnd();
            else if (menuSelection == 5)  peekFromFront();
            else if (menuSelection == 6)  peekFromEnd();
            else if (menuSelection == 7)  insertTo();
            else if (menuSelection == 8)  removeFrom();
            else if (menuSelection == 9)  get();
            else if (menuSelection == 10) getLength();
            else if (menuSelection == 11) clear();
            else                          return;

            // Tell the user what the deque looks like now
            System.out.println("Deque is:");
            System.out.println(q.toString());
            System.out.println();
        }
    }

    /**
     * Get an integer from the user, handling non-integer inputs
     * @param prompt the string to use to prompt the user for an integer
     * @return the integer value entered by the user
     */
    private static int getUserInt(String prompt) {
        while (true) {
            System.out.println(prompt);
            String input = myScanner.nextLine();
            Scanner intScanner = new Scanner(input);
            if (intScanner.hasNextInt()) {
                return intScanner.nextInt();
            } else System.out.println("Not an integer: " + intScanner.nextLine());
        }
    }

    /**
     * Lead the user through adding an integer to the end of the deque
     */
    private static void addToEnd() {
        if (q.length() < IDeque.MAX_LENGTH) {
            q.enqueue(getUserInt("What integer to enqueue to the end of the Deque?"));
        } else System.out.println(dequeFullErrorString);
    }

    /**
     * Lead the user through adding an integer to the front of the deque
     */
    private static void addToFront() {
        if (q.length() < IDeque.MAX_LENGTH) {
            q.inject(getUserInt("What integer to inject to the front of the Deque?"));
        } else System.out.println(dequeFullErrorString);
    }

    /**
     * Lead the user through removing an integer to the front of the deque
     */
    private static void removeFromFront() {
        if (q.length() > 0) {
            System.out.println("Character at the front: " + q.dequeue());
        } else System.out.println(dequeEmptyErrorString);
    }

    /**
     * Lead the user through removing an integer to the back of the deque
     */
    private static void removeFromEnd() {
        if (q.length() > 0) {
            System.out.println("Character at the end: " + q.removeLast());
        } else System.out.println(dequeEmptyErrorString);
    }

    /**
     * Lead the user through looking at an integer at the front of the deque
     */
    private static void peekFromFront() {
        if (q.length() > 0) {
            System.out.println("Peek: " + q.peek());
        } else System.out.println(dequeEmptyErrorString);
    }

    /**
     * Lead the user through looking at an integer at the back of the deque
     */
    private static void peekFromEnd() {
        if (q.length() > 0) {
            System.out.println("EndOfDeque: " + q.endOfDeque());
        } else System.out.println(dequeEmptyErrorString);
    }

    /**
     * Lead the user through inserting an integer at an arbitrary position in the deque
     */
    private static void insertTo() {
        if (q.length() < IDeque.MAX_LENGTH) {
            int input = getUserInt("What integer to insert to the Deque?");
            int pos;
            while (true) {
                pos = getUserInt("What position to insert in?");
                if (0 < pos && pos <= q.length() + 1) break;
                System.out.println(dequePositionErrorString);
            }
            q.insert(input, pos);
        } else System.out.println(dequeFullErrorString);
    }

    /**
     * Lead the user through removing an integer from an arbitrary position in the deque
     */
    private static void removeFrom() {
        if (q.length() > 0) {
            int pos;
            while (true) {
                pos = getUserInt("What position to remove from the Deque?");
                if (0 < pos && pos <= q.length()) break;
                System.out.println(dequePositionErrorString);
            }
            System.out.println(q.remove(pos) + " was at position " + pos + " in the Deque.");
        } else System.out.println(dequeEmptyErrorString);
    }

    /**
     * Lead the user through looking at an integer at an arbitrary position in the deque
     */
    private static void get() {
        if (q.length() > 0) {
            int pos;
            while (true) {
                pos = getUserInt("What position to get from the Deque?");
                if (0 < pos && pos <= q.length()) break;
                System.out.println(dequePositionErrorString);
            }
            System.out.println(q.get(pos) + " is at position " + pos + " in the Deque.");
        } else System.out.println(dequeEmptyErrorString);
    }

    /**
     * Show the user the length of the deque
     */
    private static void getLength() {
        System.out.println("Length of Deque: " + q.length());
    }

    /**
     * Clear the deque of items and inform the user
     */
    private static void clear() {
        q.clear();
        System.out.println("Deque is now empty!");
    }
}
