package cpsc2150.MyDeque;

/**
 * Abstract implementation of the IDeque interface
 *  provides a toString() method to child classes.
 */
public abstract class AbsDeque<T> implements IDeque<T> {
    /**
     * Generates a string representation of the class
     * @return string representation of this class;
     * @post AbsDeque contains the same elements before and after the operation
     */
    @Override
    public String toString() {
        String rep = "<";
        for (int i = 1; i <= length(); i++) {
            rep = rep + get(i);
            if (i < length()) rep = rep + ", ";
        }
        rep = rep + ">";
        return rep;
    }
}
