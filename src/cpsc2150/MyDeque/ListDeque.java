package cpsc2150.MyDeque;

import java.util.*;

/**
 * Correspondence length = myQ.size()
 * Correspondence self = myQ
 *
 * @invariant 0 <= myQ.size() <= MAX_LENGTH
 *
 * @author Jacob Collins
 * @author Nicholas Childers
 */
public class ListDeque<T> extends AbsDeque<T> {
    //this time store the deque in a list
    // myQ.get(0) is the front of the deque
    private final List<T> myQ;

    /**
     * @post myQ is an empty T list
     */
    public ListDeque() {
        this.myQ = new LinkedList<>();
    }

    public void enqueue(T x) {
        myQ.add(x);
    }

    public T dequeue() {
        return myQ.remove(0);
    }

    public void inject(T x) {
        myQ.add(0, x);
    }

    public T removeLast() {
        return myQ.remove(myQ.size() - 1);
    }

    public int length() {
        return myQ.size();
    }

    public void clear() {
        myQ.clear();
    }
}
