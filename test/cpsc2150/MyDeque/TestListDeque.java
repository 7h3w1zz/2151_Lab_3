package cpsc2150.MyDeque;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests for ListDeque
 *
 * @author Jacob Collins
 * @author Nicholas Childers
 */
public class TestListDeque {
    private IDeque<Character> MakeADeque() {
        return new ListDeque<>();
    }

    /** Test enqueue **/

    @Test
    public void testEnqueue_empty() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<a>";

        // Test
        q.enqueue('a');

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testEnqueue_full() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        int elements = IDeque.MAX_LENGTH;

        // Desired result
        String result = "<a";
        for (int i = 0; i < elements - 1; i++) result += ", a";
        result += ">";

        // Test
        for (int i = 0; i < elements; i++) q.enqueue('a');

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testEnqueue_order() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<T, e, s, t>";

        // Test
        q.enqueue('T');
        q.enqueue('e');
        q.enqueue('s');
        q.enqueue('t');

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }


    /** Test dequeue **/

    @Test
    public void testDequeue_return() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        Character result = 'T';

        q.enqueue('T');

        // Make sure we got the desired result
        assertEquals(result, q.dequeue());
    }

    @Test
    public void testDequeue_removal() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<e, s, t>";

        q.enqueue('T');
        q.enqueue('e');
        q.enqueue('s');
        q.enqueue('t');

        // Test
        q.dequeue();

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testDequeue_single_element() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<>";

        q.enqueue('T');

        // Test
        q.dequeue();

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }


    /** Test inject **/

    @Test
    public void testInject_empty() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<a>";

        // Test
        q.inject('a');

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testInject_full() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        int elements = IDeque.MAX_LENGTH;

        // Desired result
        String result = "<a";
        for (int i = 0; i < elements - 1; i++) result += ", a";
        result += ">";

        // Test
        for (int i = 0; i < elements; i++) q.inject('a');

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testInject_order() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<t, s, e, T>";

        // Test
        q.inject('T');
        q.inject('e');
        q.inject('s');
        q.inject('t');

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }


    /** Test removeLast **/

    @Test
    public void testRemoveLast_return() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        Character result = 'T';

        q.enqueue('T');

        // Make sure we got the desired result
        assertEquals(result, q.removeLast());
    }

    @Test
    public void testRemoveLast_removal() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<T, e, s>";

        q.enqueue('T');
        q.enqueue('e');
        q.enqueue('s');
        q.enqueue('t');

        // Test
        q.removeLast();

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testRemoveLast_single_element() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<>";

        q.enqueue('T');

        // Test
        q.removeLast();

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }


    /** Test clear **/

    @Test
    public void testClear_empty() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<>";

        q.clear();

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testClear_single_element() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<>";

        q.enqueue('a');

        q.clear();

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testClear_full() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<>";

        for (int i = 0; i < IDeque.MAX_LENGTH; i++) q.enqueue('a');

        q.clear();

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }


    /** Test peek **/

    @Test
    public void testPeek_return() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        Character result = 'T';

        q.enqueue('T');
        q.enqueue('e');
        q.enqueue('s');
        q.enqueue('t');

        // Make sure we got the desired result
        assertEquals(result, q.peek());
    }

    @Test
    public void testPeek_removal() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<T, e, s, t>";

        q.enqueue('T');
        q.enqueue('e');
        q.enqueue('s');
        q.enqueue('t');

        // Test
        q.peek();

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testPeek_single_element() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        Character result = 'T';

        q.enqueue('T');

        // Make sure we got the desired result
        assertEquals(result, q.peek());
    }


    /** Test endOfDeque **/

    @Test
    public void testEndOfDeque_return() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        Character result = 't';

        q.enqueue('T');
        q.enqueue('e');
        q.enqueue('s');
        q.enqueue('t');

        // Make sure we got the desired result
        assertEquals(result, q.endOfDeque());
    }

    @Test
    public void testEndOfDeque_removal() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<T, e, s, t>";

        q.enqueue('T');
        q.enqueue('e');
        q.enqueue('s');
        q.enqueue('t');

        // Test
        q.endOfDeque();

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testEndOfDeque_single_element() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        Character result = 'T';

        q.enqueue('T');

        // Test
        q.endOfDeque();

        // Make sure we got the desired result
        assertEquals(result, q.endOfDeque());
    }


    /** Test insert **/

    @Test
    public void testInsert_middle() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<a, b, c>";

        q.enqueue('a');
        q.enqueue('c');

        // Test
        q.insert('b', 2);

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testInsert_full() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        int elements = IDeque.MAX_LENGTH;

        // Desired result
        String result = "<a";
        for (int i = 0; i < elements - 1; i++) result += ", a";
        result += ">";

        // Test
        for (int i = 0; i < elements; i++) q.insert('a', 1);

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testInsert_end() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<T, e, s, t>";

        q.enqueue('T');
        q.enqueue('e');
        q.enqueue('s');

        // Test
        q.insert('t', 4);

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }


    /** Test remove **/

    @Test
    public void testRemove_return() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        Character result = 'b';

        q.enqueue('a');
        q.enqueue('b');
        q.enqueue('c');

        // Make sure we got the desired result
        assertEquals(result, q.remove(2));
    }

    @Test
    public void testRemove_middle() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<a, c>";

        q.enqueue('a');
        q.enqueue('b');
        q.enqueue('c');

        // Test
        q.remove(2);

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testRemove_single_element() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<>";

        q.enqueue('T');

        // Test
        q.remove(1);

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }


    /** Test get **/

    @Test
    public void testGet_return() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        Character result = 'b';

        q.enqueue('a');
        q.enqueue('b');
        q.enqueue('c');

        // Make sure we got the desired result
        assertEquals(result, q.get(2));
    }

    @Test
    public void testGet_order() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        String result = "<a, b, c>";

        q.enqueue('a');
        q.enqueue('b');
        q.enqueue('c');

        // Test
        q.get(2);

        // Make sure we got the desired result
        assertEquals(result, q.toString());
    }

    @Test
    public void testGet_single_element() {
        // Create IDeque
        IDeque<Character> q = MakeADeque();

        // Desired result
        Character result = 'T';

        q.enqueue('T');

        // Make sure we got the desired result
        assertEquals(result, q.get(1));
    }
}
